import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

class UserArrow
{
   private double[] xpoints;
   private double[] ypoints;
   public Polygon shape; //Stores the arrow after being moved
   private double x, y;
   private double[] origX;
   private double[] origY;

   //9mm at 25 and 18mm at 50
   private final double GROWSPEED = 0.0013;
   private double size;

   public UserArrow(int setX, int setY)
   {
      reset(setX, setY);
   }

   public void reset(int setX, int setY)
   {
      x = setX;
      y = setY;
      xpoints = new double[7];
      xpoints[0] = 0;
      xpoints[1] = 5;
      xpoints[2] = (6/3);
      xpoints[3] = (6/3);
      xpoints[4] = 0-(6/3);
      xpoints[5] = 0-(6/3);
      xpoints[6] = -5;
      ypoints = new double[7];
      ypoints[0] = -5;
      ypoints[1] = 0;
      ypoints[2] = 0;
      ypoints[3] = 5;
      ypoints[4] = 5;
      ypoints[5] = 0;
      ypoints[6] = 0;
      shape = new Polygon();
      /*shape.addPoint(0, -5);
        shape.addPoint(5, -1);
        shape.addPoint(1, -1);
        shape.addPoint(1, 5);
        shape.addPoint(-2, 5);
        shape.addPoint(-2, -1);
        shape.addPoint(-5, -1);*/
      origX = new double[7];
      origY = new double[7];
      for (int i = 0; i < 7; i++)
      {
         shape.addPoint((int) xpoints[i], (int) ypoints[i]);
         origX[i] = xpoints[i];
         origY[i] = ypoints[i];
      }
      size = 1;
   }

   public void doUpdates(double setX, double setY)
   {
      //Reposition
      x = setX;
      y = setY;
      //Resize
      size += GROWSPEED;
      shape = new Polygon();
      for (int i = 0; i < 7; i++)
      {
         xpoints[i] = origX[i] * size;
         ypoints[i] = origY[i] * size;
         shape.addPoint((int) (xpoints[i] + x), (int) (ypoints[i] + y));
      }
   }

   public double getX()
   {
      return x;
   }

   public double getY()
   {
      return y;
   }
}
