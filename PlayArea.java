import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.awt.image.*;

class PlayArea extends JPanel
{
   private UserArrow ship;
   private EnemyArrow[] enemies;
   private int score = 0;

   private final int areaWidth;
   private final int areaHeight;
   private final int NUM_OF_ENEMIES;

   PlayArea(UserArrow theShip, EnemyArrow[] enemies, int wWidth, int wHeight, int enemyNums)
   {
      ship = theShip;
      this.enemies = enemies;
      areaWidth = wWidth;
      areaHeight = wHeight - 25; //Window height - top box height
      NUM_OF_ENEMIES = enemyNums;
      setPreferredSize(new Dimension(areaWidth, areaHeight));
      //setBorder(BorderFactory.createLineBorder(Color.black));
      setVisible(true);
   }

   public void paint(Graphics g)
   {
      update(g);
   }

   public void update(Graphics g)
   {
      g.clearRect(0, 0, areaWidth, areaHeight);
      g.setColor(new Color(255, 138, 96));
      g.fillPolygon(ship.shape);
      for (int i = 0; i < NUM_OF_ENEMIES; i++)
      {
         g.drawImage(enemies[i].drawnShape(), (int) enemies[i].getX(), (int) enemies[i].getY(), this);
      }
      g.setColor(new Color(0, 0, 0));
      g.setFont(new Font(Font.DIALOG, Font.PLAIN, 7 + (score / 4)));
      FontMetrics fm = g.getFontMetrics();
      g.drawString("" + score,
            (int) ship.getX() - (fm.stringWidth("" + score) / 2),
            (int) ship.getY() + (fm.getAscent() / 2));
      g.drawRect(0, 0, areaWidth - 1, areaHeight - 1);
   }

   public void setScore(int score)
   {
      this.score = score;
   }
}
