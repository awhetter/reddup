import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

class Reddup implements Runnable, ActionListener, MouseMotionListener
{
   private static Reddup program;
   private JFrame w;
   private int score;
   private int scoreCount;
   private boolean playing = false;
   private PlayArea playArea;
   private int sinceScored = 0;

   private JLabel scoreBoard;
   private JButton playButton;
   private UserArrow ship;
   private EnemyArrow[] enemies;

   private final static int windowWidth = 625;
   private final static int windowHeight = 425;
   private final int NUM_OF_ENEMIES = 40;
   private final int MOVES_UNTIL_SCORE = 90;

   // Create a new Ruddup object and start the graphics thread
   public static void main (String[] args)
   {
      //Anti-alias fonts
      System.setProperty("awt.useSystemAAFontSettings","on");
      System.setProperty("swing.aatext", "true");

      program = new Reddup();
      SwingUtilities.invokeLater(program);
   }

   public void run()
   {
      w = new JFrame("My Game");
      w.setDefaultCloseOperation(w.EXIT_ON_CLOSE);
      w.add(topBox(), BorderLayout.PAGE_START);
      // w.add(arena());
      Point mousePos = MouseInfo.getPointerInfo().getLocation();
      ship = new UserArrow(mousePos.x, mousePos.y);
      enemies = new EnemyArrow[NUM_OF_ENEMIES];
      for (int i = 0; i < NUM_OF_ENEMIES; i++)
      {
         enemies[i] = new EnemyArrow(Math.random() * windowWidth,
               Math.random() * windowHeight, Math.random() * Math.PI * 2,
               windowWidth, windowHeight);
      }
      playArea = new PlayArea(ship, enemies, windowWidth, windowHeight, NUM_OF_ENEMIES);
      playArea.addMouseMotionListener(this);
      playArea.setLayout(null);
      scoreBoard = new JLabel("Score: " + 0);
      playArea.add(scoreBoard);
      w.add(playArea);
      w.pack();
      w.setLocationByPlatform(true);
      w.setVisible(true);
      score = 0;
   }

   private Box topBox()
   {
      Box box = Box.createHorizontalBox();
      box.setPreferredSize(new Dimension(windowWidth, 25));
      box.add(new JLabel("This is my game. Go play it."));
      Box buttonPad = Box.createHorizontalBox();
      buttonPad.setPreferredSize(new Dimension(windowWidth, 25));
      playButton = new JButton("Play");
      playButton.addActionListener(this);
      playButton.setActionCommand("play");
      box.add(buttonPad);
      box.add(playButton);
      return box;
   }

   // Perform actions when the menu buttons are pressed.
   public void actionPerformed (ActionEvent e)
   {
      if (!playing)
      {
         if (e.getActionCommand().equals("play"))
         {
            playButton.setVisible(false);
            score = 0;
            sinceScored = 0;
            ship.reset(0, 0);
            playing = true;
         }
      }
   }

   public void mouseDragged(MouseEvent e)
   {
      mouseMoved(e);
   }

   public void mouseMoved(MouseEvent e)
   {
      if (playing)
      {
         ship.doUpdates((int) e.getPoint().getX(),
               (int) e.getPoint().getY());
         scoreBoard.setLocation((int) e.getPoint().getX(),
               (int) e.getPoint().getY());
         // Update enemies
         for (int i = 0; i < NUM_OF_ENEMIES; i++)
         {
            enemies[i].doUpdates();
         }

         playArea.setScore(score);
         playArea.repaint();

         //Check for collisions
         for (int i = 0; i < NUM_OF_ENEMIES; i++)
         {
            /* Check if colliding with the bounding box
             * before checking every pixel */
            if(ship.shape.intersects((int) enemies[i].getX(), (int) enemies[i].getY(), 10, 10))
            {
skipIfDead:
               for (int x = 0; x < 10; x++)
               {
                  for (int y = 0; y < 10; y++)
                  {
                     if (enemies[i].drawnShape().getRGB(x, y) != 0)
                     {
                        if (ship.shape.intersects(enemies[i].getX() + x, enemies[i].getY() + y, 1, 1))
                        {
                           gameOver(score);
                           break skipIfDead;
                        }
                     }
                  }
               }
            }
         }
      }
      if (playing)
      {
         //Not dead so add to score
         sinceScored++;
         if (sinceScored >= MOVES_UNTIL_SCORE)
         {
            sinceScored = 0;
            score++;
            scoreBoard.setText("Score: " + score);
         }
      }
   }

   public void gameOver(int score)
   {
      playing = false;
      playButton.setVisible(true);
   }

   // Do any cleaning and exit the whole program.
   public static void quit()
   {
      System.exit(0);
   }
}
