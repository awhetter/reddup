import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;

class EnemyArrow
{
   public Polygon shape; //Stores the arrow after being moved
   private double x, y, angle;

   private BufferedImage bi;
   private Graphics2D big;

   private final double MOVESPEED = 0.5;
   private final int windowWidth;
   private final int windowHeight;

   public EnemyArrow(double setX, double setY, double angle, int wWidth, int wHeight)
   {
      x = setX;
      y = setY;
      this.angle = angle;
      windowWidth = wWidth;
      windowHeight = wHeight;
      shape = new Polygon();
      shape.addPoint(5, 0);
      shape.addPoint(10, 5);
      shape.addPoint(7, 5);
      shape.addPoint(7, 10);
      shape.addPoint(3, 10);
      shape.addPoint(3, 5);
      shape.addPoint(0, 5);
      bi =  new BufferedImage(10,10, BufferedImage.TYPE_INT_ARGB);
      big = bi.createGraphics();
      big.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
      big.setColor(new Color(148, 148, 255, 255));
      big.setTransform(AffineTransform.getRotateInstance(angle,5,5));
      big.fillPolygon(shape);
   }

   public void doUpdates()
   {
      /* 4 | 1
       * -----
       * 3 | 2 */
      //Reposition
      double theta;
      if (angle < (Math.PI / 2))
      {
         theta = (Math.PI / 2) - angle;
         x += MOVESPEED * Math.cos(theta);
         y -= MOVESPEED * Math.sin(theta);
      }
      else if (angle < Math.PI)
      {
         theta = angle - (Math.PI / 2);
         x += MOVESPEED * Math.cos(theta);
         y += MOVESPEED * Math.sin(theta);
      }
      else if (angle < (3 * Math.PI) / 2)
      {
         theta = ((3 * Math.PI) / 2) - angle;
         x -= MOVESPEED * Math.cos(theta);
         y += MOVESPEED * Math.sin(theta);
      }
      else
      {
         theta = angle - ((3 * Math.PI) / 2);
         x -= MOVESPEED * Math.cos(theta);
         y -= MOVESPEED * Math.sin(theta);
      }

      //Teleport onto other side of play area if needed
      if (x < (0 - 10))
      {
         x = windowWidth;
      }
      else if (x > windowWidth)
      {
         x = 0 - 10;
      }
      if (y < 0 - 10)
      {
         y = windowHeight;
      }
      else if (y > windowHeight)
      {
         y = 0 - 10;
      }
   }

   public BufferedImage drawnShape()
   {
      return bi;
   }

   public double getX()
   {
      return x;
   }

   public double getY()
   {
      return y;
   }
}
